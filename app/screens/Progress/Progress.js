import React from "react";
import { StyleSheet, View, Text } from "react-native";

export default function Progress() {
  return (
    <View style={styles.container}>
      <Text>Estamos en Mi Progreso</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});