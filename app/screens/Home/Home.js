import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { Icon } from "react-native-elements";
import Button from "../../components/Button";
import { withNavigation } from "react-navigation";
import AssignedOrders from "./AssignedOrders";

function Home(props) {
  const { navigation } = props;
  const [isDisponible, setIsDisponible] = useState(false);
  const [showPedidos, setShowPedidos] = useState(false);

  function ContentHome() {
    return (
      <View
        style={{
          alignItems: "center",
          marginLeft: 10,
          marginRight: 10,
        }}
      >
        <TouchableOpacity
          onPress={() =>
            isDisponible ? setShowPedidos(true) : console.log("nada")
          }
        >
          <Image
            style={styles.containerImage}
            source={
              !isDisponible
                ? require("../../../assets/img/home/no-disponible.png")
                : require("../../../assets/img/home/si-disponible.png")
            }
            resizeMode="contain"
          />
        </TouchableOpacity>

        {isDisponible ? (
          <View
            style={{
              alignItems: "center",
            }}
          >
            <Text style={{ color: "#f80000" }}>
              **Notificación automática en desarrollo:
            </Text>
            <Text style={{ color: "#f80000" }}>
              Click en la imagen para continuar
            </Text>
          </View>
        ) : (
          <Text></Text>
        )}

        {!isDisponible ? (
          <Text style={styles.textMessage}>Comienza a recibir pedidos</Text>
        ) : (
          <View
            style={{
              alignItems: "center",
            }}
          >
            <Text style={styles.textMessage}>Atento!</Text>
            <Text style={styles.textSubMessage}>
              Te estaremos asignando un pedido
            </Text>
            <Text style={styles.textSubMessage}>en cualquier momento</Text>
          </View>
        )}
      </View>
    );
  }

  return (
    <ScrollView centerContent={true}>
      <View style={styles.container}>
        <Button
          type="gradient"
          text="PONTE DISPONIBLE"
          onPress={() => {
            setIsDisponible(!isDisponible);
            isDisponible ? setShowPedidos(false) : console.log("nada");
          }}
          disable={isDisponible}
        />

        <View style={styles.containerHora}>
          <Icon
            type="material-community"
            name="clock-outline"
            size={30}
            color={"#22C873"}
          />
          <Text style={styles.textHora}> 6:54 PM</Text>
        </View>

        <View>
          {!isDisponible ? (
            <ContentHome />
          ) : !showPedidos ? (
            <ContentHome />
          ) : (
            <AssignedOrders />
          )}
        </View>
      </View>
    </ScrollView>
  );
}

export default withNavigation(Home);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 10,
  },
  containerHora: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 20,
  },
  textHora: {
    fontSize: 30,
    color: "#22C873",
  },
  containerImage: {
    marginBottom: 50,
    marginTop: 80,
    width: 200,
    height: 200,
  },
  textMessage: {
    color: "#173254",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: 24,
    letterSpacing: 0.5,
  },
  textSubMessage: {
    color: "#173254",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: 16,
    letterSpacing: 0.5,
  },
});
