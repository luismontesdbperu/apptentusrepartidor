import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import { withNavigation } from "react-navigation";
import ListOrders from "../../components/ListOrders";
import Button from "../../components/Button";

function AssignedOrders(props) {
  const { navigation } = props;

  return (
    <View style={styles.container}>
      <View
        style={{
          alignItems: "center",
          marginTop: 20,
          marginBottom: 20,
        }}
      >
        <Image
          style={styles.imageFooter}
          source={require("../../../assets/img/login/image-footer.png")}
          resizeMode="contain"
        />
        <Text
          style={{
            marginTop: 10,
            fontSize: 16,
            fontWeight: "bold",
            color: "#173254",
          }}
        >
          Tienes 3 pedidos asignados
        </Text>
      </View>

      <Button type="g" text="MOSTRAR QR" />

      <ListOrders />
    </View>
  );
}

export default withNavigation(AssignedOrders);

const styles = StyleSheet.create({
  imageFooter: {
    height: 120,
    width: 120,
    marginTop: 10,
  },
  container: {
    alignItems: "center",
  },
});
