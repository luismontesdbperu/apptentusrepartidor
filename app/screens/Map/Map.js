import React from "react";
import { StyleSheet, View, Text } from "react-native";
import Button from "../../components/Button";
import { withNavigation } from "react-navigation";

function Map(props) {
  const { navigation } = props;
  return (
    <View style={styles.container}>
      <Text>Estamos en el Mapa</Text>
      <Button
        type="g"
        text="CERRAR SESION"
        onPress={() => navigation.navigate("Login")}
      />
    </View>
  );
}

export default withNavigation(Map);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
