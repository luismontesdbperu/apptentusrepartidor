import React from "react";
import { StyleSheet, View, Text, Image, TouchableOpacity } from "react-native";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
} from "native-base";
import { LinearGradient } from "expo-linear-gradient";
import { Icon } from "react-native-elements";
import { withNavigation } from "react-navigation";
import Button from "../../components/Button";

function RecoverPassword(props) {
  const { navigation } = props;

  return (
    <View style={styles.containerLogin}>
      <Image
        source={require("../../../assets/img/tentus/tentus.png")}
        style={styles.tentus}
      />

      <View style={styles.containerLabelInput}>
        <Text style={styles.textLabelInput}>
          Ingresa el email para recuperar tu contraseña
        </Text>
      </View>
      <Item regular style={styles.input}>
        <Input placeholder="Email" />
        <Icon
          iconStyle={styles.icon}
          active
          type="material-community"
          name="email-outline"
        />
      </Item>

      <Button type="g" text="ENVIAR SOLICITUD" />

      <Button
        text="REGRESAR AL LOGIN"
        onPress={() => navigation.navigate("Login")}
      />
    </View>
  );
}

export default withNavigation(RecoverPassword);

const styles = StyleSheet.create({
  containerLogin: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    width: "80%",
    borderColor: "#105EAD",
    borderWidth: 1,
    borderRadius: 45,
    margin: 10,
    padding: 3,
  },
  icon: {
    color: "#105EAD",
    marginRight: 10,
  },
  tentus: {
    marginTop: 30,
    marginBottom: 30,
  },
  containerOlvidaste: {
    width: "80%",
  },
  containerLabelInput: {
    width: "80%",
  },
  textLabelInput: {
    textAlign: "left",
    color: "#0F5DAC",
    fontSize: 15,
    letterSpacing: 1.2,
  },
});
