import React, { useState } from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import { Item, Input } from "native-base";
import { Icon } from "react-native-elements";
import { withNavigation } from "react-navigation";
import Button from "../../components/Button";

function Login(props) {
  const { navigation } = props;
  const [hidePassword, setHidePassword] = useState(true);

  return (
    <View style={styles.containerLogin}>
      <Image
        source={require("../../../assets/img/tentus/tentus.png")}
        style={styles.tentus}
      />

      <Item regular style={styles.input}>
        <Input placeholder="Email" />
        <Icon
          iconStyle={styles.icon}
          active
          type="material-community"
          name="email-outline"
        />
      </Item>

      <Item regular style={styles.input}>
        <Input placeholder="Contraseña" secureTextEntry={hidePassword} />
        <Icon
          iconStyle={styles.icon}
          active
          type="material-community"
          name={hidePassword ? "eye-outline" : "eye-off-outline"}
          onPress={() => setHidePassword(!hidePassword)}
        />
      </Item>

      <View style={styles.containerOlvidaste}>
        <Text
          style={styles.textOlvidaste}
          onPress={() => navigation.navigate("RecoverPassword")}
        >
          ‎¿Olvidaste tu contraseña?
        </Text>
      </View>

      <Button
        type="gradient"
        text="INGRESA AHORA"
        onPress={() => navigation.navigate("NavigationTabs")}
      />

      <View>
        <Image
          style={styles.imageFooter}
          source={require("../../../assets/img/login/image-footer.png")}
          resizeMode="contain"
        />
      </View>
    </View>
  );
}

export default withNavigation(Login);

const styles = StyleSheet.create({
  containerLogin: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    width: "80%",
    borderColor: "#105EAD",
    borderWidth: 1,
    borderRadius: 45,
    margin: 10,
    padding: 3,
  },
  icon: {
    color: "#105EAD",
    marginRight: 10,
  },
  tentus: {
    marginTop: 30,
    marginBottom: 30,
  },
  containerOlvidaste: {
    width: "70%",
    marginTop: 20,
    marginBottom: 20,
  },
  textOlvidaste: {
    textAlign: "right",
    color: "#0F5DAC",
    fontSize: 15,
    letterSpacing: 1.2,
    textDecorationLine: "underline",
  },
  imageFooter: {
    height: 200,
    width: 200,
    marginTop: 20,
  },
});
