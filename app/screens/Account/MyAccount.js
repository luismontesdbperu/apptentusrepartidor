import React from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import { withNavigation } from "react-navigation";
import Button from "../../components/Button";

function MyAccount(props) {
  const { navigation } = props;

  return (
    <View style={styles.container}>
      <Text>Estamos en Mi Perfil</Text>

      <Button
        type="g"
        text="CERRAR SESION"
        onPress={() => navigation.navigate("Login")}
      />
    </View>
  );
}

export default withNavigation(MyAccount);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
