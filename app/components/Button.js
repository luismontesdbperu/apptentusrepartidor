import React from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import { LinearGradient } from "expo-linear-gradient";

const Button = (props) => {
  // <Summary>
  // El componente Button recibe 3 parametros
  // type: "gradient" para usar color tipo gradiente, si no se llama a este parametro tipo, se usa color solid por defecto
  // text: texto del button
  // onPress: en esta variable se pasa la funcion que va a ejecutar el button al presionar
  // </Summary>

  const { type, text, onPress } = props;
  const { disable } = props;

  return (
    <TouchableOpacity style={styles.buttonGradient} onPress={onPress}>
      <LinearGradient
        colors={
          disable
            ? ["#CDD3DA", "#CDD3DA"]
            : type == "g" || type == "gradient"
            ? ["#78EFA4", "#0F5DAC"]
            : ["#0F5DAC", "#0F5DAC"]
        }
        style={styles.gradient}
        start={[0, 0]}
        end={[1.1, 0]}
      >
        <View>
          <Text style={styles.text}>{text}</Text>
        </View>
      </LinearGradient>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  gradient: {
    justifyContent: "center",
    height: 50,
    borderRadius: 25,
  },
  buttonGradient: {
    margin: 10,
    elevation: 2,
    shadowColor: "#000",
    shadowOpacity: 0.8,
    shadowOffset: {
      height: 1,
      width: 1,
    },
    borderRadius: 25,
  },
  text: {
    marginLeft: 30,
    marginRight: 30,
    color: "white",
    fontWeight: "bold",
    fontSize: 12,
    letterSpacing: 4.1,
    textAlign: "center",
  },
});
