import React from "react";
import {
  View,
  StyleSheet,
  FlatList,
  Text,
  TouchableOpacity,
} from "react-native";
import Constants from "expo-constants";
import { Icon } from "react-native-elements";

const DATA = [
  {
    key: "12120054",
    idPedido: "12120054",
    tienda: "Franco Supermercados - Aviación",
    hora: "4:15 PM",
  },
  {
    key: "12120055",
    idPedido: "12120055",
    tienda: "Franco Supermercados - Aviación",
    hora: "4:30 PM",
  },
  {
    key: "12120056",
    idPedido: "12120056",
    tienda: "Franco Supermercados - Aviación",
    hora: "4:42 PM",
  },
];

function Item(props) {
  const { item } = props;

  return (
    <View style={styles.item}>
      {/* Vista datos izquierda */}
      <View
        style={{
          width: "50%",
        }}
      >
        <Text style={styles.title}>ID DEL PEDIDO</Text>
        <Text style={styles.title}>#{item.idPedido}</Text>
        <Text style={styles.subtitle}>{item.tienda}</Text>
      </View>
      {/* Vista hora derecha */}
      <View
        style={{
          width: "50%",
        }}
      >
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "flex-end",
          }}
        >
          <Icon
            type="material-community"
            name="clock-outline"
            iconStyle={{
              size: 18,
              color: "#60CEA5",
            }}
          />
          <Text
            style={{
              color: "#60CEA5",
              fontWeight: "bold",
              fontSize: 18,
            }}
          >
            {" "}
            {item.hora}
          </Text>
        </View>
      </View>
    </View>
  );
}

export default function ListOrders() {
  //   const { onPress } = props;
  return (
    <View
      style={{
        width: "100%",
      }}
    >
      <FlatList
        data={DATA}
        renderItem={({ item }) => <Item item={item} />}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
  },
  item: {
    backgroundColor: "#fff",
    padding: 20,
    marginLeft: 16,
    marginRight: 16,
    marginBottom: 5,
    flex: 1,
    flexDirection: "row",
    borderRadius: 10,
  },
  title: {
    fontSize: 14,
  },
  subtitle: {
    fontSize: 12,
    color: "#757F8C",
    marginTop: 8,
  },
});
