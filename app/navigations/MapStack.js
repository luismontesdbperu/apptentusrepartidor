import { createStackNavigator } from "react-navigation-stack";
import MapScreen from "../screens/Map/Map";

const HomeScreenStacks = createStackNavigator({
  Map: {
    screen: MapScreen,
    navigationOptions: () => ({
      title: "Mapa",
    }),
  },
});

export default HomeScreenStacks;
