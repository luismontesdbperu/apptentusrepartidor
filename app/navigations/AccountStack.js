import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import MyAccountScreen from "../screens/Account/MyAccount";
import Login from "../screens/Account/Login";
import RecoverPassword from "../screens/Account/RecoverPassword";
import NavigationScreen from "../navigations/Navigation";

const AccountScreenStacks = createStackNavigator({
  Login: {
    screen: Login,
    navigationOptions: () => ({
      header: null,
    }),
  },
  RecoverPassword: {
    screen: RecoverPassword,
    navigationOptions: () => ({
      header: null,
    }),
  },
  MyAccount: {
    screen: MyAccountScreen,
    navigationOptions: () => ({
      title: "Mi Perfil",
    }),
  },
  NavigationTabs: {
    screen: NavigationScreen,
    navigationOptions: () => ({
      header: null,
    }),
  },
});

export default createAppContainer(AccountScreenStacks);
