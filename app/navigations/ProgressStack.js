import { createStackNavigator } from "react-navigation-stack";
import ProgressScreen from "../screens/Progress/Progress";

const ProgressScreenStacks = createStackNavigator({
  Progress: {
    screen: ProgressScreen,
    navigationOptions: () => ({
      title: "Mi Progreso",
    }),
  },
});

export default ProgressScreenStacks;
