import { createStackNavigator } from "react-navigation-stack";
import HistoryScreen from "../screens/OrdersHistory/History";

const OrdersHistoryScreenStacks = createStackNavigator({
  OrdersHistory: {
    screen: HistoryScreen,
    navigationOptions: () => ({
      title: "Historial de Pedidos",
    }),
  },
});

export default OrdersHistoryScreenStacks;
