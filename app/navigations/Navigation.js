import React from "react";
import { Icon } from "react-native-elements";
import { createAppContainer } from "react-navigation";
import { createBottomTabNavigator } from "react-navigation-tabs";

import HomeScreenStacks from "./HomeStack";
import OrdersHistoryScreenStack from "./OrdersHistoryStack";
import ProgressScreenStack from "./ProgressStack";
import AccountScreenStacks from "./AccountStack";
import MapScreenStacks from "./MapStack";

const NavigationStacks = createBottomTabNavigator(
  {
    Home: {
      screen: HomeScreenStacks,
      navigationOptions: () => ({
        tabBarLabel: "Inicio",
        tabBarIcon: ({ tintColor }) => (
          <Icon
            type="material-community"
            name="home-outline"
            size={30}
            color={tintColor}
          />
        ),
      }),
    },
    MyOrdersHistory: {
      screen: OrdersHistoryScreenStack,
      navigationOptions: () => ({
        tabBarLabel: "Historial",
        tabBarIcon: ({ tintColor }) => (
          <Icon
            type="material-community"
            name="truck"
            size={30}
            color={tintColor}
          />
        ),
      }),
    },
    Progress: {
      screen: ProgressScreenStack,
      navigationOptions: () => ({
        tabBarLabel: "Progreso",
        tabBarIcon: ({ tintColor }) => (
          <Icon
            type="material-community"
            name="finance"
            size={30}
            color={tintColor}
          />
        ),
      }),
    },
    Map: {
      screen: MapScreenStacks,
      navigationOptions: () => ({
        tabBarLabel: "Mapa",
        tabBarIcon: ({ tintColor }) => (
          <Icon
            type="material-community"
            name="map-marker-outline"
            size={30}
            color={tintColor}
          />
        ),
      }),
    },
  },
  {
    initialRouteName: "Home",
    order: ["Home", "MyOrdersHistory", "Progress", "Map"],
    tabBarOptions: {
      showLabel: false,
      inactiveTintColor: "#A6AAB4",
      activeTintColor: "#318da9",
      style: {
        backgroundColor: "#fff",
        height: 80,
      },
    },
  }
);
//A6AAB4 plomo
// colors={["#78EFA4", "#0F5DAC"]}
export default createAppContainer(NavigationStacks);
