import { createStackNavigator } from "react-navigation-stack";
import HomeScreen from "../screens/Home/Home";
import OrdersInProcessScreen from "../screens/Home/AssignedOrders";

const HomeScreenStacks = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: () => ({
      title: "Inicio",
    }),
  },
  OrdersInProcess: {
    screen: OrdersInProcessScreen,
    navigationOptions: () => ({
      title: "Pedidos Tomados",
    }),
  },
});

export default HomeScreenStacks;
